package io.litesync.test;

import android.app.AlertDialog;
import android.app.Application;
import android.content.DialogInterface;
import android.util.Log;

import java.io.File;

import org.sqlite.database.sqlite.SQLiteDatabase;
import org.sqlite.database.sqlite.SQLiteStatement;
import org.sqlite.database.sqlite.SQLiteException;

public class MyApplication extends Application {

    SQLiteDatabase db = null;

    MainActivity currentActivity = null;

    @Override
    public void onCreate() {
        super.onCreate();
        ConnectToDb();
    }

    public SQLiteDatabase getConnection() {
        return db;
    }

    public void setCurrentActivity(MainActivity activity) {
        this.currentActivity = activity;
    }

    public void ConnectToDb() {
        try {

            System.loadLibrary("litesync");

            File dbfile = new File(getFilesDir(), "dbtest.db");
            //File dbfile = currentActivity.getApplicationContext().getDatabasePath("dbtest.db");
            String uriStr = "file:" + dbfile.getAbsolutePath() + "?node=secondary&connect=tcp://192.168.1.45:1234";

            Log.d("uri", uriStr);

            db = SQLiteDatabase.openOrCreateDatabase(uriStr, null);

            Log.d("LiteSync Test", "waiting the database to be ready");

            db.onReady(() -> {
                Log.d("LiteSync Test", "the database is ready - thread id: " + Thread.currentThread().getId());
		        prepare_db();
                if (currentActivity != null) {
                    currentActivity.on_db_ready();
                }
            });

            db.onSync(() -> {
                Log.d("LiteSync Test", "on_db_sync - thread id: " + Thread.currentThread().getId());
                if (currentActivity != null) {
                    currentActivity.onUpdateClick();
                }
            });

        } catch (SQLiteException ex) {
            showMessage("DB open: " + ex.toString());
        } catch (java.lang.Exception ex) {
            showMessage("ConnectToDb: " + ex.toString());
        }
    }

    /*
    public boolean db_is_ready() {

        try {
            // retrieve the status
            SQLiteStatement statement = db.compileStatement("PRAGMA sync_status");
            String result = statement.simpleQueryForString();
            statement.close();
            // parse the JSON result
            JSONObject jObject = new JSONObject(result);
            return jObject.getBoolean("db_is_ready");
        } catch (SQLiteException ex) {
            showMessage("prepare statement: " + ex.toString());
            db.close();
            return false;
        } catch (java.lang.Exception ex) {
            showMessage("db_is_ready: " + ex.toString());
            return false;
        }

    }
    */


    public void prepare_db() {

        try {
            db.execSQL("CREATE TABLE IF NOT EXISTS tasks (id INTEGER PRIMARY KEY,name,done)");
        } catch (SQLiteException ex) {
            showMessage("create table: " + ex.toString());
            db.close();
        }

    }


    public void showMessage(String msg){
        //Snackbar snackbar = Snackbar.make(coordinatorLayout, "Welcome to AndroidHive", Snackbar.LENGTH_LONG);
        //snackbar.show();

        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle("Error");
        alertDialog.setMessage(msg);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();

    }

}
